import { validateId, validateProblemType } from "./utils.js";

const validateRequestBody = (body) => {
  const { drone_id, problem_type } = body;

  if (!drone_id || !problem_type) {
    return { isValid: false, error: 'Both fields drone_id and problem_type are required.' };
  }

  const droneIdValidation = validateId(drone_id);
  if (!droneIdValidation.isValid) {
    return { isValid: false, error: `Drone ID: ${droneIdValidation.error}` };
  }

  const problemTypeValidation = validateProblemType(problem_type);
  if (!problemTypeValidation.isValid) {
    return { isValid: false, error: `Destination: ${problemTypeValidation.error}` };
  }

  return { isValid: true };
};

const validateMaintenanceRequestMiddleware = (req, res, next) => {
  const validation = validateRequestBody(req.body);

  if (validation.isValid) {
    next();
  } else {
    res.status(400).json({ error: validation.error });
  }
};

export { validateMaintenanceRequestMiddleware };