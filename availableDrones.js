import { validateId } from "./utils.js";

const validateStationIdMiddleware = (req, res, next) => {
    const { stationId } = req.body;
    const validation = validateId(stationId);

    if (validation.isValid) {
        next();
    } else {
        res.status(400).json({ error: validation.error });
    }
};

const getAvailableDrones = (stationId) => {
    // generate random number between 1 and 10
    return {
        available_drones: Math.floor(Math.random() * 10) + 1
    };
};

export {getAvailableDrones, validateStationIdMiddleware};