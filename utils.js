// Description: This file contains utility functions for validating data.

const VALID_PROBLEM_TYPES = ['mechanical', 'electrical', 'software', 'other'];

const validateId = (id) => {
  // Check if stationId is a non-empty string
  if (typeof id === 'number' && id > 0 && id < 100) {
    return { isValid: true };
  } else {
    return { isValid: false, error: 'Station ID must be a number between 0 and 100' };
  }
};

const validateProblemType = (problemType) => {
    if (VALID_PROBLEM_TYPES.includes(problemType)) {
        return { isValid: true };
    } else {
        return { isValid: false, error: 'Invalid problem type' };
    }
};

export {validateId, validateProblemType};