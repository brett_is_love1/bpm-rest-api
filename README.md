# BPM - Rest API

## Requirements
- Node.js 16+

## Used libraries
- Express
- Nodemon

## Installation
```
npm install
```

## Starting the app
```
npm run dev
```
The rest API is accessible at [http://localhost:8081/]().

## Endpoints

There are three endpoints available
- **~/calculate-route**

    Accepts data in the following JSON format:
    ```json
    {
    "lon": "The longitude coordinate of the destination.",
    "lat": "The latitude coordinate of the destination.",
    }
    ```
    `lon` and `lat`: both must be valid longitude and latitude coordinates (i.e. longitude between -180 and 180, and latitude between -90 and 90).

- **~/available-drones**

    Accepts data in the following JSON format:
    ```json
    {
    "station_id": "The identification number of the docking station.",
    }
    ```
    `station_id`: must be a number between 0 and 100.
- **~/request-maintenance**

    Accepts data in the following JSON format:
    ```json
    {
    "drone_id": "The identification number of the drone.",
    "problem_type": "Description of the problem based on the system's evaluation."
    }
    ```
    `drone_id`: must be a number between 0 and 100.

    `problem_type`: parameter can take values `mechanical`, `electrical`, `software` or `other`.

## How to test endpoints

All of the endpoints are called using the `POST` method so they are not accessible through a web browser. In order to send data to the endpoint, you need to work from terminal and use `curl` with the following structure:
```bash
curl -X POST http://localhost:8081/api/{endpoint} -H "Content-Type: application/json" -d '{JSON}'
```

Examples:
```bash
# calculate route endpoint
curl -X POST http://localhost:8081/api/calculate-route \
  -H "Content-Type: application/json" \
  -d '{
    "lon": 40.7128,
    "lat": -54.0060
  }'
```
```bash
# available drones endpoint
curl -X POST http://localhost:8081/api/available-drones \
  -H "Content-Type: application/json" \
  -d '{
    "stationId": 2
  }'
```
```bash
# request maintenance endpoint
curl -X POST http://localhost:8081/api/request-maintenance \
  -H "Content-Type: application/json" \
  -d '{
    "drone_id": 3,
    "problem_type": "mechanical"
  }'
```
