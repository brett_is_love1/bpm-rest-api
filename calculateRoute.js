// Function to calculate the route
const calculateRoute = (destination) => {
  const toRadians = (degrees) => degrees * (Math.PI / 180);

  const lon1 = 34.0522;
  const lat1 = -40.2437;
  console.log(lon1, lat1);
  const { lon: lon2, lat: lat2 } = destination;

  const R = 6371; // Radius of the Earth in kilometers
  const dLat = toRadians(lat2 - lat1);
  const dLon = toRadians(lon2 - lon1);

  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(toRadians(lat1)) * Math.cos(toRadians(lat2)) *
    Math.sin(dLon / 2) * Math.sin(dLon / 2);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

  const distance = R * c; // Distance in kilometers

  // Mock route information
  return {
    distance: `${distance.toFixed(2)} km`,
    duration: `${(distance / 60).toFixed(2)} hours`, // Assuming an average speed of 60 km/h
  };
};

export { calculateRoute };