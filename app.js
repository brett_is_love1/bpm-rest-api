import express from 'express';
import {calculateRoute} from './calculateRoute.js';
import { getAvailableDrones, validateStationIdMiddleware } from './availableDrones.js';
import { validateMaintenanceRequestMiddleware } from './requestMaintenance.js';


const app = express();
const port = 8081;

app.use(express.json());

// Default API endpoint: GET /api
app.get('/api', (req, res) => {
    res.send('Hello from the API!');
});

// Endpoint calculate-route: POST /api/calculate-route
app.post('/api/calculate-route', (req, res) => {
    // const { start, destination } = req.body;
    console.log('calculate route');
    const route = calculateRoute(req.body);
    res.json(route);
});

// Endpoint available-drones: POST /api/available-drones
app.post('/api/available-drones', validateStationIdMiddleware, (req, res) => {
    const {stationId} = req.body;
    const availableDrones = getAvailableDrones(stationId);
    res.json(availableDrones);
});

// Endpoint request-maintenance: POST /api/request-maintenance
app.post('/api/request-maintenance', validateMaintenanceRequestMiddleware, (req, res) => {
    const { drone_id, problem_type } = req.body;
    res.json({
        message: `Maintenance team for drone ${drone_id} with problem: ${problem_type}, is on the way.`
    });
});

// 404 handler
app.use((req, res, next) => {
    res.status(404).send('Sorry, that route does not exist.');
});

// Start the server
app.listen(port, () => {
    console.log(`Server is running on http://localhost:${port}`);
});
